// Copyright (c) 2016, Serenity Contributors
//
// Permission to use, copy, modify, and/or distribute this software for any purpose
// with or without fee is hereby granted, provided that the above copyright notice
// and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
// REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
// INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
// OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
// TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
// THIS SOFTWARE.

//! Requires the 'framework' feature flag be enabled in your project's
//! `Cargo.toml`.
//!
//! This can be enabled by specifying the feature in the dependency section:
//!
//! ```toml
//! [dependencies.serenity]
//! git = "https://github.com/serenity-rs/serenity.git"
//! features = ["framework", "standard_framework"]
//! ```

use serenity::{
    async_trait,
    client::bridge::gateway::ShardManager,
    model::{
        event::ResumedEvent,
        gateway::Ready,
        interactions::{Interaction, InteractionData},
    },
    prelude::*,
};
use tokio::time::sleep;
use InteractionData::ApplicationCommand;

use std::{env, str::FromStr, sync::Arc, time::Duration};

use serde_json::{json, Value};
use tracing::{error, info};
use tracing_subscriber::{EnvFilter, FmtSubscriber};

use crate::trello::{Named, Trello};

pub struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}

struct Handler;

fn env_var<A: FromStr>(key: &str, name: &str) -> A
where
    <A as FromStr>::Err: std::fmt::Debug,
{
    env::var(key)
        .unwrap_or_else(|_| panic!("Expected a {} in the environment", name))
        .parse()
        .unwrap_or_else(|_| panic!("could not parse {}", name))
}

fn name_query(name: &str) -> [(&'static str, String); 2] {
    [
        ("idList", env_var("TRELLO_LIST_ID", "trello list id")),
        ("name", name.to_owned()),
    ]
}

#[async_trait]
impl EventHandler for Handler {
    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        if let Some(ApplicationCommand(command)) = &interaction.data {
            let content = match &*command.name {
                "ping" => "pong",
                "buy" => {
                    if let Some(Value::String(name)) = &command.options[0].value {
                        let trello =
                            Trello::new_with_env().expect("couldn't create trello instance");
                        trello
                            .post::<Named>(trello.endpoint("cards", &name_query(name)))
                            .await
                            .expect("couldn't create card in list");

                        "\u{2705}"
                    } else {
                        "failed"
                    }
                }
                a => unreachable!("cannot find command with {:?}", a),
            };
            interaction
                .create_interaction_response(&ctx.http, |response| {
                    response.interaction_response_data(|message| message.content(content))
                })
                .await
                .expect("Interaction failed, f in chat please");

            sleep(Duration::from_secs(env_var(
                "SUCCESS_DELETE",
                "success delete",
            )))
            .await;
            interaction
                .delete_original_interaction_response(&ctx.http)
                .await
                .expect("could not delete original interaction message");
        }
    }
    async fn ready(&self, ctx: Context, ready: Ready) {
        info!("Connected as {}", ready.user.name);

        let guild_id = env_var("DISCORD_GUILD_ID", "discord guild id");
        ctx.http
            .create_guild_application_command(
                guild_id,
                &json!({
                    "name": "buy",
                    "description": "put item on shopping list",
                    "options": [
                        {
                            "type": 3,
                            "name": "name",
                            "description": "What to add to the list",
                            "required": true
                        }
                    ]
                }),
            )
            .await
            .expect("couldn't create ping command");
        ctx.http
            .create_guild_application_command(
                guild_id,
                &json!({
                    "name": "ping",
                    "description": "pong",
                }),
            )
            .await
            .expect("couldn't create ping command");

        let interactions = ctx.http.get_guild_application_commands(guild_id).await;

        if let Ok(xs) = interactions {
            info!(
                "I have the following guild slash commands: {:?}",
                xs.iter().map(|x| &x.name).collect::<Vec<_>>()
            );
        }
    }

    async fn resume(&self, _: Context, _: ResumedEvent) {
        info!("Resumed");
    }
}

pub async fn bot() {
    // This will load the environment variables located at `./.env`, relative to
    // the CWD. See `./.env.example` for an example on how to structure this.
    dotenv::dotenv().expect("Failed to load .env file");

    // Initialize the logger to use environment variables.
    //
    // In this case, a good default is setting the environment variable
    // `RUST_LOG` to debug`.
    let subscriber = FmtSubscriber::builder()
        .with_env_filter(EnvFilter::from_default_env())
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("Failed to start the logger");

    let token: String = env_var("DISCORD_TOKEN", "token");

    let appid: u64 = env_var("DISCORD_APPLICATION_ID", "application id ");

    let mut client = Client::builder(token)
        .event_handler(Handler)
        .application_id(appid)
        .await
        .expect("Err creating client");

    {
        let mut data = client.data.write().await;
        data.insert::<ShardManagerContainer>(client.shard_manager.clone());
    }

    let shard_manager = client.shard_manager.clone();

    tokio::spawn(async move {
        tokio::signal::ctrl_c()
            .await
            .expect("Could not register ctrl+c handler");
        shard_manager.lock().await.shutdown_all().await;
    });

    if let Err(why) = client.start().await {
        error!("Client error: {:?}", why);
    }
}
