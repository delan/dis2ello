mod bot;
mod trello;

use std::env::args;

use crate::bot::bot;
use crate::trello::{print_boards, print_lists};

#[tokio::main]
async fn main() -> eyre::Result<()> {
    dotenv::dotenv()?;

    let mut args = args().skip(1).take(2);

    match (args.next().as_deref(), args.next().as_deref()) {
        (Some("boards"), _) => print_boards().await?,
        (Some("lists"), Some(id)) => print_lists(id).await?,
        _ => bot().await,
    };

    Ok(())
}
