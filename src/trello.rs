use std::borrow::Borrow;
use std::cmp::Ordering;
use std::collections::{BTreeMap, BTreeSet};
use std::env;

use reqwest::{Response, Url};
use serde::de::DeserializeOwned;

#[derive(Debug)]
pub struct Trello {
    base: Url,
}

impl Trello {
    pub fn new(key: &str, token: &str) -> eyre::Result<Self> {
        let base = Url::parse_with_params(
            "https://api.trello.com/1/",
            &[("key", key), ("token", token)],
        )?;

        Ok(Self { base })
    }

    pub fn new_with_env() -> eyre::Result<Self> {
        Self::new(&env::var("TRELLO_API_KEY")?, &env::var("TRELLO_API_TOKEN")?)
    }

    pub async fn get<Res: DeserializeOwned>(&self, address: Url) -> eyre::Result<Res> {
        Ok(self.get0(address).await?.error_for_status()?.json().await?)
    }

    pub async fn get0(&self, address: Url) -> eyre::Result<Response> {
        Ok(reqwest::get(address).await?)
    }

    pub async fn post<Res: DeserializeOwned>(&self, address: Url) -> eyre::Result<Res> {
        Ok(self
            .post0(address)
            .await?
            .error_for_status()?
            .json()
            .await?)
    }

    pub async fn post0(&self, address: Url) -> eyre::Result<Response> {
        Ok(reqwest::Client::new().post(address).send().await?)
    }

    pub fn simple_endpoint(&self, path: &str) -> Url {
        let mut result = self.base.clone();
        result.set_path(&[result.path(), path].join(""));

        result
    }

    // FIXME E0698 when query is &[] (or None with query: Oq: Into<Option<Q>>)
    pub fn endpoint<Q, Qk, Qv>(&self, path: &str, query: Q) -> Url
    where
        Q: IntoIterator,
        Qk: AsRef<str>,
        Qv: AsRef<str>,
        <Q as IntoIterator>::Item: Borrow<(Qk, Qv)>,
    {
        let mut result = self.simple_endpoint(path);
        result.query_pairs_mut().extend_pairs(query);

        result
    }
}

#[derive(Debug, serde::Deserialize)]
pub struct Named {
    pub id: String,
    pub name: String,
}

#[derive(Debug, Eq, serde::Deserialize)]
#[allow(non_snake_case)]
pub struct Board {
    pub id: String,
    pub name: String,
    pub idOrganization: Option<String>,
}

impl PartialEq for Board {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Ord for Board {
    fn cmp(&self, other: &Self) -> Ordering {
        self.id.cmp(&other.id)
    }
}

impl PartialOrd for Board {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub async fn print_boards() -> eyre::Result<()> {
    let trello = Trello::new_with_env()?;
    let address = trello.simple_endpoint("members/me/boards");
    let mut orgs = BTreeMap::<Option<String>, BTreeSet<Board>>::default();

    for board in trello.get::<BTreeSet<Board>>(address).await? {
        orgs.entry(board.idOrganization.clone())
            .or_default()
            .insert(board);
    }

    for (id, boards) in orgs {
        if let Some(id) = id {
            println!("org {}", id);
        } else {
            println!("personal boards");
        }
        for board in boards {
            println!("\tboard {}: {:?}", board.id, board.name);
        }
    }

    Ok(())
}

pub async fn print_lists(board_id: impl AsRef<str>) -> eyre::Result<()> {
    let trello = Trello::new_with_env()?;
    let mut address = trello.simple_endpoint("boards");

    address
        .path_segments_mut()
        .expect("address is never a cannot-be-a-base URL")
        .extend(&[board_id.as_ref(), "lists"]);

    for list in trello.get::<Vec<Named>>(address).await? {
        println!("list {}: {:?}", list.id, list.name);
    }

    Ok(())
}
