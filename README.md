# dis2ello

1. Connect to Discord and Trello
   1. Copy .env.example to .env
   1. [Create a Discord application](https://discord.com/developers/applications)
   1. In the **Bot** section, click **Add Bot**, then follow the prompts
   1. **Copy** the bot’s token, then paste it next to DISCORD_TOKEN= in your .env
   1. **Copy** the application id, then paste it next to the DISCORD_APPLICATION_ID= in your .env
   1. **Copy** the server id inside discord by right clicking the server and clicking Copy ID, then paste it next to the DISCORD_GUILD_ID= in your .env
   1. Scroll down to **Privileged Gateway Intents**, then enable **PRESENCE INTENT**
   1. [Create a Trello API key](https://trello.com/app-key)
   1. Copy the **Key**, then paste it next to TRELLO_API_KEY= in your .env
   1. Click **manually generate a Token**, then follow the prompts
   1. Copy the token, then paste it next to TRELLO_API_TOKEN= in your .env
1. Invite your bot to a server
   1. [Go to your application](https://discord.com/developers/applications)
   1. In the **General Information** section, click **Copy** under **CLIENT ID**
   1. Go to [https://discord.com/oauth2/authorize?scope=bot&client_id=**CLIENT**&permissions=**PERMISSIONS**](https://discord.com/oauth2/authorize?scope=bot&client_id=CLIENT&permissions=PERMISSIONS)
      - **CLIENT**: paste what you copied above
      - **PERMISSIONS**: [11328](https://discordapi.com/permissions.html#11328) if SUCCESS_DELETE is set, [3136](https://discordapi.com/permissions.html#3136) otherwise
   1. Choose a server, then follow the prompts
1. Choose a list to create cards under
   1. Find your desired board id: `cargo run boards`
   1. Find your desired list id: `cargo run lists <board id>`
   1. Copy a list id, then paste it next to TRELLO_LIST_ID= in your .env
1. Start your bot: `cargo run`
1. ;buy freesh avocadoo (or ;help)

## Requirements

- Rust 1.43+ (tested with 1.43.0)
